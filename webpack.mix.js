let mix = require('laravel-mix');

// Lokalizacje
let publicDist = './wp-content/themes/internetmedia/dist';
let jsSrc = './wp-content/themes/internetmedia/dev/js/app.js';
let jsDist = './wp-content/themes/internetmedia/dist/js/';
let cssSrc = './wp-content/themes/internetmedia/dev/sass/style.scss';
let cssDist = 'wp-content/themes/internetmedia/dist/css/';
let vendCssSrc = './wp-content/themes/internetmedia/dev/sass/vendors.scss';
let vendCssDist = './wp-content/themes/internetmedia/dist/css/'

mix.setPublicPath(publicDist);

if (!mix.inProduction()) {
    mix.webpackConfig({
        devtool: 'source-map'
    }).sourceMaps();
};

mix.autoload({
    jquery: ['$', 'window.jQuery']
});

mix.js(jsSrc, jsDist)
    .extract([
        '@fancyapps/fancybox'
    ]);

mix.sass(cssSrc, cssDist)
    .options({
        processCssUrls: false
    }).disableNotifications();

mix.sass(vendCssSrc, vendCssDist);
