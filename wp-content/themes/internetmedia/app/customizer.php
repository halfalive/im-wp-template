<?php

	function internetmedia_customize_register($wp_customize) {
		$wp_customize->remove_panel('nav_menus');
		$wp_customize->remove_panel('widgets');
		$wp_customize->remove_section('static_front_page');
		$wp_customize->remove_section('custom_css');

		
	}

	add_action('customize_register', 'internetmedia_customize_register');
