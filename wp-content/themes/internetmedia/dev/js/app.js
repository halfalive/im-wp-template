import $ from 'jquery';
window.$ = window.jQuery = $;
import Cookie from './Cookie';

let cookie = new Cookie(true);
cookie.init();

$('.wpcf7-list-item-label a').click(function (e) {
    e.preventDefault();
});

$('.gallery').each(function (i, s) {
    var $this = $(this);
    var id = $this.attr('id');
    var figcaption = $this.find('figcaption');
    if (figcaption.length > 0) {
        figcaption.each(function () {
            var figitem = $(this);
            var figtext = figitem[0].outerText;
            figitem.parent().find('a').attr('data-caption', figtext)
        });
    }
    $this.find('a').attr('data-fancybox', id);
});
