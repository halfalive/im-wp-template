</div><!-- site-content -->

<footer class="footer">
    <div class="footer__body"></div>
    <div class="footer__copyright">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="m-0">
                        © <?php echo date('Y'); ?>&nbsp;<?php echo bloginfo('name'); ?>. Wszelkie prawa zastrzeżone.
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>

</div><!-- site -->
<?php get_template_part('partials/cookies') ?>
<?php wp_footer(); ?>

</body>
</html>
