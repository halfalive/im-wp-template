<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/fontawesome.css">
</head>
<?php wp_head(); ?>

<body <?php body_class(); ?>>
<div class="site">
	<?php get_template_part('partials/social'); ?>
	<?php get_template_part('partials/nav') ?>


	<div class="site-content">
