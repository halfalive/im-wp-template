<?php get_header(); ?>

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="inner-content">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; else : ?>
                        <p><?php esc_html_e( 'Strona w budowie' ); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>   

<?php get_footer(); ?>
