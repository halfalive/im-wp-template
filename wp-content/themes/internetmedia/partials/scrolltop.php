<?php

global $scroll_top;

if($scroll_top) : ?>
    <div class="scrolltop">
        <i class="fas fa-angle-up"></i>
    </div>
<?php endif; ?>