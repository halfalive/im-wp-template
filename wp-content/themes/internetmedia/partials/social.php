<?php if($social_bar): ?>

    <div id="social_links">
        <?php if ( get_field('facebook', 'option') ) : ?>
            <a id="facebookSidebar" class="open" href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
        <?php endif; ?> 
    </div>

<?php endif; ?>